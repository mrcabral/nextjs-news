import React, { useState } from "react";
import Layout from "../components/Layout";
import axios from "axios";
import Router from "next/router";

const News = ({news}) => {
    // const [searchQuery, setSearchQuery] = useState("banger");
    const [value, setValue] = useState({
        text: 'react',
        coolMsg: '',
    })

    const { text, coolMsg } = value;

    const handleChange = name => e => {
        // setSearchQuery(e.target.value);
        setValue({...value, [name] : e.target.value})
    }

    const handleSubmit = e => {
        e.preventDefault();
        Router.push(`/news/?searchTerm=${text}`);
    }

    const searchForm = () => {
        return (
            <form onSubmit={handleSubmit}>
                <input type="text" value={text} onChange={handleChange('text')} />
                <input type="text" placeholder="Escreva algo..." onChange={handleChange('coolMsg')}/>
                <button type="submit">Search</button>
            </form>
        )
    }

    return (
        <Layout mainTitle="News">
            <div>
                <h2>Lista de notícias</h2>
                <hr/>
                {coolMsg}
                <hr/>
                {searchForm()}
                <hr/>
                {news.map((n, i) => {
                    return (
                        <p key={i}>
                            <a target="_blank" href={n.url}>
                                {n.title}
                            </a>
                        </p>
                    )
                })}
            </div>
        </Layout>
    );
}

News.getInitialProps = async ({query}) => {
    let news;
    // setLoading(true);


    await axios.get(
        "https://hn.algolia.com/api/v1/search",
        {
            params: {
                query: query.searchTerm
            },
            responseType: "json"
        })
        .then(response => {
            // setLoading(false);
            news = response.data.hits;
        })
        .catch((error) => {
            console.log("ERROR", error);
            news = [];
        });

    return {news};
}

export default News;
