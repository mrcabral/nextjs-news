import Link from "next/link";
import Head from "next/head";
import Layout from "../components/Layout";

const About = () => {
    return (
        <Layout
            mainTitle="Página Sobre com Next"
            footer={`Isso foi construido pelo Marco`}
        >
            <Head>
                <title>Sobre</title>
            </Head>

            <h1>Página Sobre</h1>
            <Link href="/">
                <a>Página index</a>
            </Link>

            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla mollis congue convallis. Curabitur mattis
                mauris congue massa eleifend fermentum. Curabitur non dui in risus volutpat suscipit. Vestibulum lorem
                velit, hendrerit sit amet venenatis et, pretium at quam. Nullam pharetra tellus quis diam eleifend, non
                bibendum orci laoreet. Morbi vel diam mattis, interdum neque hendrerit, tempus neque. Etiam tempor massa
                non augue aliquet vestibulum. Curabitur eu semper velit. Nam gravida in purus at congue. Donec cursus
            </p>

            <style jsx>{`
              p {
                color: indigo;
                font-size: 20px;
              }
            `}</style>
        </Layout>
    );
}

export default About;
