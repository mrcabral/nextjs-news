import Document, {Html, Head, Main, NextScript} from "next/document";

class MyDocument extends Document {
    render() {
        return (
            <Html lang="pt">
                <Head>
                    <meta charSet="UTF-8"/>
                    <meta name="description" content="Meu React App com Next"/>
                    <meta name="Keywords" content="next react js"/>
                    <meta name="author" content="Marco Cabral"/>

                    <link rel="stylesheet"
                          href="https://cdnjs.cloudflare.com/ajax/libs/antd/4.18.5/antd.compact.min.css"
                          integrity="sha512-aduOabnwjsz7s5p8YIH9JDfrWhJUiufTUZDBzTrAoflMvfpjUoZthB1F3JYuEa4M4d7xTEQXN5+WLjF/lZE0tA=="
                          crossOrigin="anonymous" referrerPolicy="no-referrer"/>
                    <link rel="stylesheet" href="/static/style.css"/>
                </Head>

                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        );
    }
}

export default MyDocument;
