import Link from "next/link";
import Head from "next/head";
import Layout from "../components/Layout";

const Index = () => {
    return (
        <Layout
            mainTitle="Página Home com Next"
            footer={`Copyright ${new Date().getFullYear()}`}
        >
            <Head>
                <title>Home</title>
            </Head>
            <h1>Ahoy Aurokx</h1>
            <Link href="/about">
                <a>Página sobre</a>
            </Link>

            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla mollis congue convallis. Curabitur
                mattis
                mauris congue massa eleifend fermentum. Curabitur non dui in risus volutpat suscipit. Vestibulum
                lorem
                velit, hendrerit sit amet venenatis et, pretium at quam. Nullam pharetra tellus quis diam eleifend,
                non
                bibendum orci laoreet. Morbi vel diam mattis, interdum neque hendrerit, tempus neque. Etiam tempor
                massa
                non augue aliquet vestibulum. Curabitur eu semper velit. Nam gravida in purus at congue. Donec
                cursus
            </p>

        </Layout>
    );
}

export default Index;
