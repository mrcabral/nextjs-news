import Link from "next/link";

const Nav = () => {
    return (
        <div className="my-nav">
            <img src="/static/aurokx.jpg" alt="logo" height="50"/>
            <Link href="/">
                <a>Home</a>
            </Link>
            <Link href="/about">
                <a>Sobre</a>
            </Link>
            <Link href="/news">
                <a>News</a>
            </Link>

            <style jsx>{`
              a,
              img {
                padding: 10px;
              }
            `}</style>
        </div>
    );
}

export default Nav;
